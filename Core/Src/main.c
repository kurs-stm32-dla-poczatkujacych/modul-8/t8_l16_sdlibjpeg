/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "fatfs.h"
#include "libjpeg.h"
#include "sdio.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "string.h"
#include "TFT_ILI9341.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
FRESULT FatFsResult;
FATFS SdFatFs;
FIL SdCardFile;

uint8_t bytes;
char data[128];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_SPI1_Init();
  MX_SDIO_SD_Init();
  MX_FATFS_Init();
  MX_LIBJPEG_Init();
  /* USER CODE BEGIN 2 */


  ILI9341_Init(&hspi1);

  ILI9341_ClearDisplay(ILI9341_WHITE);

  //
  // FatFS mount init
  //
  FatFsResult = f_mount(&SdFatFs, "", 1);

  //
  // FatFS mount init error check
  //
  if(FatFsResult != FR_OK)
  {
  	  bytes = sprintf(data, "FatFS mount error.\n\r");
  	  HAL_UART_Transmit(&huart2, (uint8_t*)data, bytes, 1000);
  }
  else
  {
  	  bytes = sprintf(data, "FatFS mounted.\n\r");
  	  HAL_UART_Transmit(&huart2, (uint8_t*)data, bytes, 1000);

  	  //
  	  // Open file on SD for writing
  	  //
  	  FatFsResult = f_open(&SdCardFile, "test.txt", FA_WRITE|FA_CREATE_ALWAYS);

  	  //
  	  // File open error check
  	  //
  	  if(FatFsResult != FR_OK)
  	  {
  		  bytes = sprintf(data, "No test.txt file. Can't create.\n\r");
  		  HAL_UART_Transmit(&huart2, (uint8_t*)data, bytes, 1000);
  	  }
  	  else
  	  {
  		  bytes = sprintf(data, "File opened.\n\r");
  		  HAL_UART_Transmit(&huart2, (uint8_t*)data, bytes, 1000);

  		  //
		  //	Print something to this file
		  //
		  for(uint8_t i = 0; i < 10; i++)
		  {
			  f_printf(&SdCardFile, "Line number %d.\n", i);
		  }

		  //
		  // Close file
		  //
		  FatFsResult = f_close(&SdCardFile);

		  bytes = sprintf(data, "File closed.\n\r");
		  HAL_UART_Transmit(&huart2, (uint8_t*)data, bytes, 1000);


	  	  //
	  	  // Open file on SD for writing
	  	  //
	  	  FatFsResult = f_open(&SdCardFile, "test.txt", FA_READ);

	  	  //
	  	  // File open error check
	  	  //
	  	  if(FatFsResult != FR_OK)
	  	  {
	  		  bytes = sprintf(data, "No test.txt file. Can't open. \n\r");
	  		  HAL_UART_Transmit(&huart2, (uint8_t*)data, bytes, 1000);
	  	  }
	  	  else
	  	  {
	  		  UINT len;
	  		  do
	  		  {
	  			  len = 0;
		  		  f_read(&SdCardFile, data, 10, &len);
		  		  HAL_UART_Transmit(&huart2, (uint8_t*)data, len, 1000);
	  		  }while(len > 0);

			  //
			  // Close file
			  //
			  FatFsResult = f_close(&SdCardFile);
	  	  }
  	  }
  }



  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  ILI9341_ClearDisplay(ILI9341_WHITE);
	  ILI9341_DrawImageFromSD(0, 71, "logo.bin", 320, 96);

	  HAL_Delay(1000);

	  ILI9341_ClearDisplay(ILI9341_WHITE);
	  ILI9341_DrawJPEG(0, 0, "nosacz.jpg");

	  HAL_Delay(1000);


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 72;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
